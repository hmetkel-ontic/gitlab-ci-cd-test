import React from "react";

import { render } from "@testing-library/react";
import App from "./App";

// import { expect, describe, it } from "bun:test";

describe("App Component", () => {
  it("renders learn react link", () => {
    const { getByLabelText } = render(<App />);
    const inputNode1 = getByLabelText("Username");

    expect(inputNode1).toBeInTheDocument();
  });

  // it("dom test", () => {
  //   document.body.innerHTML = `<button>My button</button>`;
  //   const button = document.querySelector("button");
  //   expect(button?.innerText).toEqual("My button");
  // });
});
