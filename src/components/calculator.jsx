import React, { useState } from "react";
import "./calculator.css";

const Calculator = () => {
  const [display, setDisplay] = useState("0");
  const [currentNumber, setCurrentNumber] = useState("");
  const [previousNumber, setPreviousNumber] = useState("");
  const [operator, setOperator] = useState("");

  const handleNumberClick = (num) => {
    if (display === "0") {
      setDisplay(num.toString());
    } else {
      setDisplay(display + num);
    }
    setCurrentNumber(currentNumber + num);
  };

  const handleOperatorClick = (op) => {
    if (currentNumber !== "") {
      setDisplay((currentDisplay) => currentDisplay + " " + op + " ");
      setPreviousNumber(currentNumber);
      setCurrentNumber("");
      setOperator(op);
    }
  };

  const handleEqualsClick = () => {
    if (currentNumber !== "") {
      let result;
      switch (operator) {
        case "+":
          result = parseFloat(previousNumber) + parseFloat(currentNumber);
          break;
        case "-":
          result = parseFloat(previousNumber) - parseFloat(currentNumber);
          break;
        case "*":
          result = parseFloat(previousNumber) * parseFloat(currentNumber);
          break;
        case "/":
          result = parseFloat(previousNumber) / parseFloat(currentNumber);
          break;
        default:
          return;
      }
      setDisplay(result.toString());
      setCurrentNumber(result.toString());
      setPreviousNumber("");
      setOperator("");
    }
  };

  const handleClear = () => {
    setDisplay("0");
    setCurrentNumber("");
    setPreviousNumber("");
    setOperator("");
  };

  return (
    <div className="calculator">
      <input
        type="text"
        className="display"
        value={display}
        readOnly
        data-testid="display-input"
      />
      <div className="buttons">
        <button onClick={() => handleNumberClick(1)}>1</button>
        <button onClick={() => handleNumberClick(2)}>2</button>
        <button onClick={() => handleNumberClick(3)}>3</button>
        <button onClick={() => handleOperatorClick("+")}>+</button>
        <button onClick={() => handleNumberClick(4)}>4</button>
        <button onClick={() => handleNumberClick(5)}>5</button>
        <button onClick={() => handleNumberClick(6)}>6</button>
        <button onClick={() => handleOperatorClick("-")}>-</button>
        <button onClick={() => handleNumberClick(7)}>7</button>
        <button onClick={() => handleNumberClick(8)}>8</button>
        <button onClick={() => handleNumberClick(9)}>9</button>
        <button onClick={() => handleOperatorClick("*")}>*</button>
        <button onClick={() => handleClear()}>C</button>
        <button onClick={() => handleNumberClick(0)}>0</button>
        <button onClick={() => handleEqualsClick()}>=</button>
        <button onClick={() => handleOperatorClick("/")}>/</button>
      </div>
    </div>
  );
};

export default Calculator;
