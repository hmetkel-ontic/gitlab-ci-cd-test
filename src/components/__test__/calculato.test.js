import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Calculator from "../calculator";
// import { test, expect } from "bun:test";

test('renders calculator with initial display value "0"', () => {
  const { getByDisplayValue } = render(<Calculator />);
  const display = getByDisplayValue("0");
  expect(display).toBeInTheDocument();
});

test("clicking numbers updates display correctly", () => {
  const { getByText, getByDisplayValue } = render(<Calculator />);

  fireEvent.click(getByText("1"));
  fireEvent.click(getByText("2"));
  fireEvent.click(getByText("3"));
  const display = getByDisplayValue("123");
  expect(display).toBeInTheDocument();
});

test("clicking operators updates display correctly", () => {
  const { getByText, getByTestId } = render(<Calculator />);

  fireEvent.click(getByText("1"));
  fireEvent.click(getByText("+"));
  fireEvent.click(getByText("2"));
  fireEvent.click(getByText("="));
  const display = getByTestId("display-input");
  expect(display).toHaveValue("3");
});

test("clear button resets calculator", () => {
  const { getByText, getByTestId } = render(<Calculator />);

  fireEvent.click(getByText("1"));
  fireEvent.click(getByText("+"));
  fireEvent.click(getByText("2"));
  fireEvent.click(getByText("C"));
  const display = getByTestId("display-input");
  expect(display).toHaveValue("0");
});
