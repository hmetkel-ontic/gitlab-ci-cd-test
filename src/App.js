import "./App.css";
import Calculator from "./components/calculator";

function App() {
  return (
    <div className="App">
      <Calculator />
      <input type="text" id="username-input" />
      <label htmlFor="username-input">Username</label>
    </div>
  );
}

export default App;
