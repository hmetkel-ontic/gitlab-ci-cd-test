import { GlobalRegistrator } from "@happy-dom/global-registrator";
import { cleanup } from "@testing-library/react";
import { afterEach } from "bun:test";

export * from "@testing-library/jest-dom/jest-globals";

afterEach(() => {
  cleanup();
});

GlobalRegistrator.register();

export * from "@testing-library/react";
